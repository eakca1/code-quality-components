# MOVED TO [maintained GitLab CI Components](https://gitlab.com/components/code-quality-oss/codequality-os-scanners-integration)

# PLEASE USE the [official component](https://gitlab.com/explore/catalog/components/code-quality-oss/codequality-os-scanners-integration)

# codequality-os-scanners-integration

[GitLab Code Quality](https://docs.gitlab.com/ee/ci/testing/code_quality.html) supports integration of external tools [like ESLint](https://docs.gitlab.com/ee/ci/testing/code_quality.html#integrate-multiple-tools).

The `codequality-os-scanners-integration` GitLab component consists of a collection of CI Templates to integrate Open Source Tools with GitLab Code Quality features.

Currently integrated Code Quality Scanners are
- [Swiftlint](https://github.com/realm/SwiftLint) to scan/lint Swift code via [Swiftlint Template](templates/swiftlint.yml)
- [PMD](https://pmd.github.io/) to scan/lint Java code via [PMD Template](templates/pmd.yml)
- [Golangci Lint](https://github.com/golangci/golangci-lint) to scan/lint Go code via [Golangci Template](templates/golangci.yml)
- [pylint](https://pypi.org/project/pylint/) to scan/lint Python code via [Pylint Template](templates/pylint.yml)
- [Flake8](https://flake8.pycqa.org/en/latest/) to scan/lint Python code via [Flake8 Template](templates/flake8.yml)
- [mypy](https://mypy-lang.org/) to scan/lint Python code via [mypy Template](templates/mypy.yml)
- [eslint](https://eslint.org/) to scan/lint JS/TS code via [eslint Template](templates/eslint.yml)
- [rubocop](https://rubocop.org/) to scan/lint Ruby code via [rubocop Template](templates/rubocop.yml)
- [roslynator](https://josefpihrt.github.io/docs/roslynator/) to scan/lint C# code via [roslynator Template](templates/roslynator.yml)
- [detekt](https://detekt.dev/) to scan/lint Kotlin code via [detekt Template](templates/detekt.yml)

## Usage

The component can be used in two ways:

### 1. All-in-One Scanner Integration

Include all scanners with a single component. The component automatically detects relevant scanners based on your repository's file types (e.g., `.py` files for Python scanners).

```yaml
include:
  - component: gitlab.com/eakca1/codequality-os-scanners-integration/codequality-oss@<VERSION>
```

### 2. Individual Scanner Integration
Include specific scanners separately in your `.gitlab-ci.yml`:

```yaml
include:
  # Example of running a scanner in a separate stage
  - component: gitlab.com/eakca1/codequality-os-scanners-integration/pmd@<VERSION>
    inputs:
      stage: verify
  
  # Other language scanners (will run in 'test' stage by default)
  - component: gitlab.com/eakca1/codequality-os-scanners-integration/swiftlint@<VERSION>
  - component: gitlab.com/eakca1/codequality-os-scanners-integration/golangci@<VERSION>
  - component: gitlab.com/eakca1/codequality-os-scanners-integration/flake8@<VERSION>
  - component: gitlab.com/eakca1/codequality-os-scanners-integration/mypy@<VERSION>
  - component: gitlab.com/eakca1/codequality-os-scanners-integration/pylint@<VERSION>
  - component: gitlab.com/eakca1/codequality-os-scanners-integration/eslint@<VERSION>
  - component: gitlab.com/eakca1/codequality-os-scanners-integration/rubocop@<VERSION>
  - component: gitlab.com/eakca1/codequality-os-scanners-integration/roslynator@<VERSION>
  - component: gitlab.com/eakca1/codequality-os-scanners-integration/detekt@<VERSION>
```

## Configuration

This following section covers the configuration of `code-quality-oss` component, which wraps all code quality scanners and handles their configuration automatically. Individual component templates can be found in `templates/$componentName.yml`.

### Image Management

By default, the component uses the latest available base images and scanner versions. For example, Python-based scans use `python:latest`. You can customize this by specifying your own images that include:
- Required runtime version
- Package manager
- Pre-installed code quality scanner

### Automatic Configuration

The component automatically:
- Detects and configures appropriate scanner settings
- Identifies package managers (for Python scanners)
- Installs required scanners and dependencies

To skip automatic setup (e.g., when using custom images with pre-installed tools), set `skip_setup: true`.

### Parameters

| Parameter | Description | Default |
|-----------|-------------|---------|
| `stage` | GitLab CI/CD pipeline stage | `test` |
| `python_image` | Container image for Python-based scans | `python:latest` |
| `ruby_image` | Container image for Ruby-based scans | `ruby:latest` |
| `node_image` | Container image for ESLint scans | `node:latest` |
| `golangcilint_image` | Container image for Go scans | `golangci/golangci-lint:latest` |
| `swiftlint_image` | Container image for Swift scans | `ghcr.io/realm/swiftlint:latest` |
| `dotnet_image` | Container image for C# scans | `mcr.microsoft.com/dotnet/sdk:8.0` |
| `jdk_image` | Container image for Kotlin scans | `openjdk:11-jdk ` |
| `sarif_codeclimate_converter_image` | Container image for Sarif to Codeclimate conversion | `node:latest` |
| `skip_setup` | Bypass scanner insta‚llation and configuration | `false` |
| `codequality_component_version` | Version of underlying scan components | `0.9.2` |

### Acknowledgements

- This project uses curl and pmd images released by [GitLab CI Utils](https://gitlab.com/gitlab-ci-utils)
- For sarif to codeclimate conversion [sarif-codeclimate](https://www.npmjs.com/package/sarif-codeclimate) library is utilised. 
- Following python packages are used for conversion to codeclimate format. 
  - [mypy-gitlab-code-quality](https://pypi.org/project/mypy-gitlab-code-quality/)
  - [flake8-gl-codeclimate](https://pypi.org/project/flake8-gl-codeclimate/)
  - [pylint-gitlab](https://pypi.org/project/pylint-gitlab/)