spec:
  inputs:
    stage:
      default: test
      description: "The GitLab CI stage to run the job"
    image:
      default: python:latest # Refer to https://hub.docker.com/_/python
      description: "The image to execute mypy"
    skip_setup:
      default: false
      type: boolean
      description: "if true, it skips the package manager detection and mypy installation"
---
mypy:
  stage: $[[ inputs.stage ]]
  image: $[[ inputs.image ]]
  before_script:
    - |
      if [ "$[[ inputs.skip_setup ]]" != "true" ]; then      
        if [ -f "pyproject.toml" ]; then
          echo "Poetry project detected"
          pip install poetry
          poetry install || true
          source $(poetry env info --path)/bin/activate
        elif [ -f "requirements.txt" ]; then
          echo "Requirements file detected"
          python -m venv .venv
          source .venv/bin/activate
          pip install -r requirements.txt
        elif [ -f "setup.py" ]; then
          echo "Setuptools project detected"
          python -m venv .venv
          source .venv/bin/activate
          pip install -e .
        else
          echo "No known Python package management detected, creating a virtual environment"
          python -m venv .venv
          source .venv/bin/activate
        fi
        pip install mypy mypy-gitlab-code-quality
      fi
  script:
    - mypy $(find -type f -name "*.py" ! -path "**/.venv/**") --no-error-summary > mypy-out.txt || true  # "|| true" is used for preventing job fail when mypy find errors
    - mypy-gitlab-code-quality < mypy-out.txt > gl-code-quality-report.json
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json
    when: always
  rules:
    - if: $CODE_QUALITY_DISABLED
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" # Run code quality job in merge request pipelines
      exists:
        - "**/*.py"
    - if: $CI_OPEN_MERGE_REQUESTS  # Don't add it to a *branch* pipeline if it's already in a merge request pipeline.
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - "**/*.py"
  allow_failure: true
