spec:
  inputs:
    stage:
      default: test
      description: "The GitLab CI stage to run the job"
    image:
      default: node:latest # Refer to https://hub.docker.com/_/python
      description: "The image to execute the eslint"
    skip_setup:
      default: false
      type: boolean
      description: "if true, it skips the default eslint configuration and installation"
    ESLINT_CONFIG: 
      default: |
        module.exports = [
          {
            files: ['**/*.js', '**/*.mjs', '**/*.ts', '**/*.tsx'],
            linterOptions: {
              reportUnusedDisableDirectives: true,
            },
            languageOptions: {
              ecmaVersion: 'latest',
              sourceType: 'module',
              globals: {
                // Common globals
                module: 'readonly',
                exports: 'readonly',
                require: 'readonly',
                // Test globals
                describe: 'readonly',
                it: 'readonly'
              }
            },
            rules: {
              // Error Prevention
              'no-unused-vars': 'warn',
              'no-undef': 'error',
              'no-duplicate-imports': 'error',
              
              // Modern JavaScript practices
              'no-var': 'warn',
              'prefer-const': 'warn',
              'prefer-arrow-callback': 'warn',
              'func-names': ['warn', 'always'],
              
              // Constructor and capitalization
              'new-cap': ['warn', {
                newIsCap: true,
                capIsNew: false
              }],
              
              // Spacing and formatting
              'indent': ['warn', 4], // Changed to 4 spaces as per error log
              'no-trailing-spaces': 'warn',
              'keyword-spacing': ['error', {
                before: true,
                after: true
              }],
              'space-before-blocks': 'error',
              'space-before-function-paren': ['error', {
                anonymous: 'always',
                named: 'never',
                asyncArrow: 'always'
              }],
              
              // Replaced/updated rules
              'no-labels': 'error',
              'no-confusing-arrow': 'error',
              'no-constant-condition': 'error',
              
              // Style
              'semi': ['error', 'always'],
              'quotes': ['error', 'single'],
              'quote-props': ['warn', 'as-needed'],
              'comma-dangle': ['error', 'always-multiline'],
              'arrow-spacing': 'error'
            },
          },
        ];
      description: "Default eslint config"
---
eslint:
  image: $[[ inputs.image ]]
  stage: $[[ inputs.stage ]]
  before_script:
  - |
    if [ "$[[ inputs.skip_setup ]]" != "true" ]; then 
      if [ ! -f .eslintrc.* ]; then
        echo "No ESLint config found, creating eslint.config.js..."
        echo "$[[ inputs.ESLINT_CONFIG ]]" > eslint.config.js
      fi
      if [ -f yarn.lock ]; then
        echo "Using Yarn"
        yarn install --frozen-lockfile
      else
        echo "Using npm"
        npm ci
      fi
      if [ -f yarn.lock ]; then
        yarn add --dev eslint@latest eslint-formatter-gitlab
      else
        npm install --save-dev eslint@latest eslint-formatter-gitlab
      fi
    fi
  variables:
    ESLINT_CODE_QUALITY_REPORT:  gl-code-quality-report.json
  script:
  - |
    if [ -f yarn.lock ]; then
      yarn run eslint --format gitlab .
    else
      npx eslint --format gitlab .
    fi
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json
    when: always
  rules:
    - if: $CODE_QUALITY_DISABLED
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" # Run code quality job in merge request pipelines
      exists:
        - '**/*.js'
        - '**/*.jsx'
        - '**/*.ts'
        - '**/*.tsx'
        - '**/*.cjs'
        - '**/*.mjs'
    - if: $CI_OPEN_MERGE_REQUESTS  # Don't add it to a *branch* pipeline if it's already in a merge request pipeline.
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - '**/*.js'
        - '**/*.jsx'
        - '**/*.ts'
        - '**/*.tsx'
        - '**/*.cjs'
        - '**/*.mjs'
  allow_failure: true